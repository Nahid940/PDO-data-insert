<?php

try{
    $conn = new PDO("mysql:host=localhost;dbname=course", "root", "");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $exp) {
    return $exp->getMessage();
}

if(isset($_POST['delete'])){
	$cid=$_POST['cid'];
	$del="delete from course where id=".$cid;
    $conn->exec($del);
	header("location:view.php");
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<a href="insert.php">Insert</a>
    <table border=1>
        <tr>
            <td>Course Name</td>
            <td>Course Code</td>
            <td>Delete</td>
            <td>Update</td>
        </tr>
        <?php
		$sql = "select * from course";
		$stmt=$conn->prepare($sql);
		$stmt->execute();
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach($stmt->fetchAll() as $val) {
                ?>
                <tr>
                    <td><?php echo $val['cname'] ?></td>
                    <td><?php echo $val['ccode'] ?></td>
					<td>
						<form method="post">
							<input type="hidden" value="<?php echo $val['id']?>" name="cid">
							<input type="submit" value="Delete" name="delete">
						</form>

					</td>
                    <td><a href="edit.php?id=<?php echo $val['id']?>" >Edit</a></td>
                </tr>
                <?php
            }
        ?>
    </table>
</body>
</html>